/*     */ import javax.swing.JTable;
/*     */ 
/*     */ public class TableDemoPanel extends javax.swing.JPanel
/*     */ {
/*     */   private JTable table;
/*     */   public MyTableModel model;
/*     */   
/*     */   public TableDemoPanel(Object[][] data)
/*     */   {
/*  10 */     super(new java.awt.BorderLayout());
/*  11 */     this.model = new MyTableModel(data);
/*  12 */     this.table = new JTable(this.model);
/*     */     
/*  14 */     this.table.setPreferredScrollableViewportSize(new java.awt.Dimension(200, 500));
/*  15 */     this.table.getSelectionModel().setSelectionMode(0);
/*     */     
/*  17 */     javax.swing.JScrollPane scrollPane = new javax.swing.JScrollPane(this.table);
/*  18 */     add(scrollPane);
/*     */   }
/*     */   
/*     */   public void setAutoResizeMode(int mode) {
/*  22 */     this.table.setAutoResizeMode(mode);
/*     */   }
/*     */   
/*     */   public javax.swing.table.TableColumnModel getColumnModel() {
/*  26 */     return this.table.getColumnModel();
/*     */   }
/*     */   
/*     */   public void setModel(MyTableModel model) {
/*  30 */     this.model = model;
/*  31 */     this.table.setModel(model);
/*     */   }
/*     */   
/*     */   public int getSelectedRow() {
/*  35 */     int row = this.table.getSelectionModel().getLeadSelectionIndex();
/*  36 */     return row;
/*     */   }
/*     */   
/*     */   public int getAbsRow(int row) {
/*  40 */     return this.table.convertRowIndexToModel(row);
/*     */   }
/*     */   
/*     */   public Object[] getRowValues(int row) {
/*  44 */     int absRow = getAbsRow(row);
/*  45 */     return this.model.getRowValues(absRow);
/*     */   }
/*     */   
/*     */   public void setEnabled(boolean b) {
/*  49 */     this.table.setEnabled(b);
/*  50 */     this.table.setRowSelectionAllowed(b);
/*     */   }
/*     */   
/*     */   public void insertRow(Object[] row) {
/*  54 */     this.model.insertRow(row);
/*  55 */     this.model.fireTableRowsInserted(0, 0);
/*     */   }
/*     */   
/*     */   public Object getValueAtSelected(int column) {
/*  59 */     int row = this.table.getSelectedRow();
/*  60 */     if (row < 0)
/*     */     {
/*  62 */       javax.swing.JOptionPane.showMessageDialog(null, "Oznac song", "Chyba oznacenie", 0);
/*  63 */       return null;
/*     */     }
/*  65 */     return this.model.getValueAt(row, column);
/*     */   }
/*     */   
/*     */   public String removeRow() {
/*  69 */     int row = this.table.getSelectedRow();
/*     */     
/*     */ 
/*  72 */     if (row < 0)
/*     */     {
/*     */ 
/*  75 */       return null;
/*     */     }
/*     */     
/*  78 */     String temp = (String)this.model.getValueAt(row, 0);
/*  79 */     this.model.removeRow(row);
/*  80 */     return temp;
/*     */   }
/*     */   
/*     */   public void setColumnSelection(int start, int end) {
/*  84 */     this.table.setColumnSelectionInterval(start, end);
/*     */   }
/*     */   
/*     */   public void setRowValues(int r, Object[] row) {
/*  88 */     r = getAbsRow(r);
/*  89 */     this.model.setRowValues(r, row);
/*     */   }
/*     */   
/*     */   public void setData(Object[][] newdata) {
/*  93 */     this.model.setData(newdata);
/*     */   }
/*     */   
/*     */   public MyTableModel getModel() {
/*  97 */     return this.model;
/*     */   }
/*     */   
/*     */   public Object getValueAt(int r, int c) {
/* 101 */     return this.model.getValueAt(r, c);
/*     */   }
/*     */   
/*     */   public void setValueAtSelected(int collumn, Object value) {
/* 105 */     int row = this.table.getSelectedRow();
/* 106 */     this.table.setValueAt(value, row, collumn);
/*     */   }
/*     */   
/*     */ 
/* 110 */   public void setValueAt(int row, int col, Object data) { this.table.setValueAt(data, row, col); }
/*     */   
/*     */   public void setMe() {
/* 113 */     this.table.setRowSelectionAllowed(true);
/* 114 */     this.table.setSelectionMode(2);
/*     */   }
/*     */ }


/* Location:              M:\zuz.jar!\TableDemoPanel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */