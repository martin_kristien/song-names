# Song rename
This application can rename file names. The purpose is to allow user to change names of songs in an album automatically. Often, song files in an album contain several unnecessary data (e.g. album name, sequence number) together with the actual song name. This application can easily delete these digits/symbols of substrings from the file names.

## Use
The application loads only the folder in which it is open. Particular files can be deleted from the list of considered files. File names can be also modified manually by doulbe clicking on particular file. After all files are named as desired, click rename button to save changes.

## Running
compile the application by:
`javac Master1.java`

run the application by:
java Master1

or by double clicking on the executable *main.jar* file

## Author
The application was created by Martin Kristien in 2014.