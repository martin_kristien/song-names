/*    */ import javax.swing.event.TableModelEvent;
/*    */ import javax.swing.table.AbstractTableModel;
/*    */ 
/*    */ class MyTableModel
/*    */   extends AbstractTableModel
/*    */ {
/*    */   String[] columnNames;
/*    */   Object[][] data;
/*    */   
/*    */   public MyTableModel(Object[][] data)
/*    */   {
/* 12 */     this.data = data;
/* 13 */     this.columnNames = new String[] { "Song names" };
/*    */   }
/*    */   
/*    */   public int getColumnCount() {
/* 17 */     return this.columnNames.length;
/*    */   }
/*    */   
/*    */   public int getRowCount() {
/* 21 */     return this.data.length;
/*    */   }
/*    */   
/*    */   public String getColumnName(int col) {
/* 25 */     return this.columnNames[col];
/*    */   }
/*    */   
/*    */   public String[] getColumnNames() {
/* 29 */     return this.columnNames;
/*    */   }
/*    */   
/*    */   public Object getValueAt(int row, int col) {
/* 33 */     return this.data[row][col];
/*    */   }
/*    */   
/*    */   public Object[] getRowValues(int r) {
/* 37 */     return this.data[r];
/*    */   }
/*    */   
/*    */   public Class getColumnClass(int c) {
/* 41 */     return new String().getClass();
/*    */   }
/*    */   
/*    */   public Object[][] getData() {
/* 45 */     return this.data;
/*    */   }
/*    */   
/*    */   public void setData(Object[][] ndata) {
/* 49 */     this.data = ndata;
/* 50 */     fireTableDataChanged();
/*    */   }
/*    */   
/*    */   public void setValueAt(Object value, int row, int col) {
/* 54 */     this.data[row][col] = value;
/* 55 */     fireTableChanged(new TableModelEvent(this, row, row, col));
/*    */   }
/*    */   
/*    */   public boolean isCellEditable(int row, int column) {
/* 59 */     return true;
/*    */   }
/*    */   
/*    */   public void insertRow(Object[] newRow) {
/* 63 */     Object[][] nData = new Object[getRowCount() + 1][getColumnCount()];
/* 64 */     for (int i = 1; i < getRowCount(); i++) nData[i] = this.data[(i - 1)];
/* 65 */     nData[0] = newRow;
/* 66 */     this.data = nData;
/*    */   }
/*    */   
/*    */   public void removeRow(int row) {
/* 70 */     Object[][] nData = new Object[getRowCount() - 1][getColumnCount()];
/* 71 */     for (int i = 0; i < row; i++)
/* 72 */       nData[i] = this.data[i];
/* 73 */     for (int i = row; i < getRowCount() - 1; i++)
/* 74 */       nData[i] = this.data[(i + 1)];
/* 75 */     this.data = nData;
/* 76 */     fireTableRowsDeleted(row, row);
/*    */   }
/*    */   
/*    */   public void setRowValues(int row, Object[] newRow) {
/* 80 */     this.data[row] = newRow;
/*    */   }
/*    */ }


/* Location:              M:\zuz.jar!\MyTableModel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */