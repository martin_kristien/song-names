import java.util.ArrayList;

public class Master1 extends javax.swing.JFrame implements java.awt.event.ActionListener, javax.swing.event.TableModelListener {
  String myFolder;
  ArrayList<String> songs;
  ArrayList<java.io.File> oldFiles;
  TableDemoPanel table;
  java.awt.Container cont;
  javax.swing.JButton delete;
  javax.swing.JButton undo;
  javax.swing.JButton removeDigit;
  javax.swing.JButton removeSymbol;
  javax.swing.JButton removeString;
  javax.swing.JButton finish;
  javax.swing.JTextField f;
  
  public Master1() { super("Zuzkin program na premenovavanie pesniciek");
    setDefaultCloseOperation(3);
    this.myFolder = System.getProperty("user.dir");
    this.songs = new ArrayList();
    this.oldFiles = new ArrayList();
    loadFiles();
    setSize(400, 700);
    setResizable(false);
    setLayout(null);
    this.cont = getContentPane();
    this.table = new TableDemoPanel(generateData());
    this.table.setOpaque(true);
    this.cont.add(this.table);
    this.table.setBounds(10, 50, 200, 600);
    this.table.setVisible(true);
    this.table.getModel().addTableModelListener(this);
    this.table.setMe();
    
    this.delete = new javax.swing.JButton("remove file");
    this.delete.setBounds(225, 50, 150, 20);
    this.delete.setVisible(true);
    this.delete.addActionListener(this);
    this.cont.add(this.delete);
    
    this.removeDigit = new javax.swing.JButton("remove a digit");
    this.removeDigit.setBounds(225, 80, 150, 20);
    this.removeDigit.setVisible(true);
    this.removeDigit.addActionListener(this);
    this.cont.add(this.removeDigit);
    
    this.removeSymbol = new javax.swing.JButton("remove a symbol");
    this.removeSymbol.setBounds(225, 110, 150, 20);
    this.removeSymbol.setVisible(true);
    this.removeSymbol.addActionListener(this);
    this.cont.add(this.removeSymbol);
    
    this.removeString = new javax.swing.JButton("remove string");
    this.removeString.setBounds(225, 140, 150, 20);
    this.removeString.setVisible(true);
    this.removeString.addActionListener(this);
    this.cont.add(this.removeString);
    
    this.f = new javax.swing.JTextField();
    this.f.setBounds(225, 160, 150, 20);
    this.f.setVisible(true);
    this.cont.add(this.f);
    
    this.finish = new javax.swing.JButton("rename and exit");
    this.finish.setBounds(225, 500, 150, 20);
    this.finish.setVisible(true);
    this.finish.addActionListener(this);
    this.cont.add(this.finish);
    
    setVisible(true);
  }
  
  public Object[][] generateData() { Object[][] newData = new Object[this.songs.size()][1];
    for (int i = 0; i < this.songs.size(); i++) {
      newData[i][0] = this.songs.get(i);
    }
    return newData; }
  
  public void loadFiles() { java.io.File[] arrayOfFile;
    int j = (arrayOfFile = new java.io.File(this.myFolder).listFiles()).length; for (int i = 0; i < j; i++) { java.io.File f = arrayOfFile[i];
      if (f.isFile()) {
        this.oldFiles.add(f);
        this.songs.add(f.getName());
      }
    }
  }
  
  public void renameFinal() { boolean correct = true;
    for (String s : this.songs) {
      java.io.File renamed = new java.io.File(s);
      if (!((java.io.File)this.oldFiles.remove(0)).renameTo(renamed)) {
        correct = false;
        javax.swing.JOptionPane.showMessageDialog(null, "could not rename song: " + s, "error occured!", 0);
      }
    }
    if (correct) {
      javax.swing.JOptionPane.showMessageDialog(null, "success", "done!", 0);
    } else
      javax.swing.JOptionPane.showMessageDialog(null, "an error occured", "error occured!", 0);
  }
  
  public void remove(int i) { this.oldFiles.remove(i);
    this.songs.remove(i);
  }
  
  public static void main(String[] args) { new Master1(); }
  
  public void tableChanged(javax.swing.event.TableModelEvent e)
  {
    int row = e.getFirstRow();
    if (e.getType() == -1)
      return;
    this.songs.set(row, (String)this.table.getValueAt(row, 0));
  }
  
  public void actionPerformed(java.awt.event.ActionEvent e) {
    if (e.getSource() == this.finish) {
      renameFinal();
      System.exit(0);
    } else if (e.getSource() == this.delete) {
      for (int i = this.table.getSelectedRow(); i >= 0; i = this.table.getSelectedRow()) {
        if (this.table.removeRow() == null) return;
        this.songs.remove(i);
        this.oldFiles.remove(i);
      }
    } else if (e.getSource() == this.removeDigit) {
      removeDigit();
    } else if (e.getSource() == this.removeSymbol) {
      removeSymbol();
    } else if (e.getSource() == this.removeString) {
      String s = this.f.getText();
      if (!this.f.equals(""))
        removeString(s);
      this.f.setText("");
    }
  }
  
  public void removeDigit() { for (int i = 0; i < this.songs.size(); i++)
      if ((((String)this.songs.get(i)).charAt(0) >= '0') && (((String)this.songs.get(i)).charAt(0) <= '9')) {
        this.songs.set(i, ((String)this.songs.get(i)).substring(1));
        this.table.setValueAt(i, 0, this.songs.get(i));
      }
  }
  
  public void removeSymbol() {
    for (int i = 0; i < this.songs.size(); i++)
      if (isSymbol(((String)this.songs.get(i)).charAt(0))) {
        this.songs.set(i, ((String)this.songs.get(i)).substring(1));
        this.table.setValueAt(i, 0, this.songs.get(i));
      }
  }
  
  public boolean isSymbol(char c) {
    if ((c >= '0') && (c <= '9'))
      return false;
    if ((c >= 'a') && (c <= 'z')) return false;
    if ((c >= 'A') && (c <= 'Z')) return false;
    return true;
  }
  
  public void removeString(String s) { for (int i = 0; i < this.songs.size(); i++)
    {

      this.songs.set(i, remove((String)this.songs.get(i), s));
      this.table.setValueAt(i, 0, this.songs.get(i));
    }
  }
  
  public static String remove(String str, String sub) {
    return str.replaceFirst(sub, "");
  }
}
